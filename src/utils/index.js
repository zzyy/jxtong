/**
 * Created by zzy on 2019/2/27.
 */
import ajax from './axios'
import { setStore, getStore, removeStore } from './storage'
export {
  ajax,
  setStore,
  getStore,
  removeStore
}
