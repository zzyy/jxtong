/**
 * Created by zzy on 2019/2/27.
 */
// 保存localStorage
export const setStore = (name, content) => {
  if (!name) return false
  if (typeof content !== 'string') {
    content = JSON.stringify(content)
  }
  try {
    window.localStorage.setItem(name, content)
  } catch (oException) {
    if (oException.name === 'QuotaExceededError') {
      alert('超出本地存储限额！')
    }
  }
}

// 获取localStorage
export const getStore = name => {
  if (!name) return false
  return window.localStorage.getItem(name)
}

// 删除localStorage
export const removeStore = name => {
  if (!name) return false
  window.localStorage.removeItem(name)
}
