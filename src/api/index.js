/**
 * Created by zzy on 2019/2/18.
 */
const apiModule = {
  // 登录
  login: {
    url: '/edu-admin/app/loginMobile',
    method: 'post'
  },
  // 根据学生身份证获取详情
  childDetail: {
    url: '/edu-admin/app/person/infoStuForMob/',
    method: 'get'
  },
  // 根据学生身份证获取详情
  childDetailForPC: {
    url: '/edu-admin/app/person/infoStu/',
    method: 'get'
  },
  // 根据学生身份证激活家长
  uptParForMobByStu: {
    url: '/edu-admin/app/person/uptParForMobByStu',
    method: 'post'
  },
  // 家长身份证激活家长
  uptParForMobByOwn: {
    url: '/edu-admin/app/person/uptParForMobByOwn',
    method: 'post'
  },
  // 上传文件
  upload: {
    url: '/edu-admin/app/person/upload',
    method: 'post'
  },
  uploadBase64: {
    url: '/edu-admin/app/person/uploadForBase64',
    method: 'post'
  },
  // 通知列表
  getNoticeList: {
    url: '/edu-admin/app/workPlatform/toNotice',
    method: 'get'
  },
  // 登录人信息
  getUserInfo: {
    url: '/edu-admin/app/workPlatform/personMessage',
    method: 'get'
  },
  // 通知详情
  getNoticeDetail: {
    url: '/edu-admin/app/notice/info/',
    method: 'get'
  },
  // 被接人列表
  childList: {
    url: '/edu-admin/app/deliver/childList',
    method: 'get'
  },
  // 班级概况
  getClassBasic: {
    url: '/edu-admin/app/workPlatform/countClassS',
    method: 'post'
  },
  // 班级列表
  getClassList: {
    url: '/edu-admin/app/deliverT/getListForClass',
    method: 'post'
  },
  // 根据班级获取学生列表
  getStudentList: {
    url: '/edu-admin/app/person/listForClasses',
    method: 'post'
  },
  // 获取学生所对应的接人列表 -- 离校
  getParentByStu: {
    url: '/edu-admin/app/deliverT/parentListR/',
    method: 'get'
  },
  // 到校
  getParentByStuD: {
    url: '/edu-admin/app/deliverT/parentListD/',
    method: 'get'
  },
  // 学生离校-老师确认
  teacherConfirmR: {
    url: '/edu-admin/app/deliverT/tracherConfirmR',
    method: 'post'
  },
  // 学生到校-老师确认
  teacherConfirmD: {
    url: '/edu-admin/app/deliverT/tracherConfirmD',
    method: 'post'
  },
  // 我发起的审批-家长二次确认
  teacherApply: {
    url: '/edu-admin/app/notice/confirmList',
    method: 'post'
  },
  // 发给我的-人员审批
  personApprove: {
    url: '/edu-admin/app/person/personExamineList',
    method: 'post'
  },
  // 人员审批操作
  controlPersonAppr: {
    url: '/edu-admin/app/person/isExamine',
    method: 'post'
  },
  // 发给我的-代接送
  shuttleApprove: {
    url: '/edu-admin/app/shuttleapply/list',
    method: 'post'
  },
  // 家长、老师修改电话
  uptPhone: {
    url: '/edu-admin/app/person/uptPhoneForMob',
    method: 'post'
  },
  // 家长端
  // 获取审批列表
  getApproveList: {
    url: '/edu-admin/app/deliver/statusList',
    method: 'post'
  },
  // 审批详情
  getApproveDetail: {
    url: '/edu-admin/app/deliver/info/',
    method: 'get'
  },
  // 审批操作
  approveConfirm: {
    url: '/edu-admin/app/deliver/statusConfirm',
    method: 'post'
  },
  // 获取接送人列表
  getShuttleList: {
    url: '/edu-admin/app/deliver/appList',
    method: 'post'
  },
  // 家长查看子女列表
  childByParent: {
    url: '/edu-admin/app/person/listByPar',
    method: 'post'
  },
  // 子女详情
  getChildDetail: {
    url: '/edu-admin/app/person/stuInfoForMob/',
    method: 'get'
  },
  // 家长子女关系绑定
  relationBind: {
    url: '/edu-admin/app/person/updateProgram',
    method: 'post'
  },
  // 子女查看家长列表
  parentListByStu: {
    url: '/edu-admin/app/person/listByStu',
    method: 'post'
  },
  // 家长详情
  parentDetails: {
    url: '/edu-admin/app/person/infoPar/',
    method: 'get'
  },
  // 新增家长
  addParent: {
    url: '/edu-admin/app/person/addPar',
    method: 'post'
  },
  // 代接申请
  djApply: {
    url: '/edu-admin/app/deliver/applyR',
    method: 'post'
  },
  // 代送申请
  dsApply: {
    url: '/edu-admin/app/deliver/applyD',
    method: 'post'
  },
  // 代接送申请详情
  applyDetails: {
    url: '/edu-admin/app/deliver/applyInfo/',
    method: 'get'
  },
  // 同校代接家长列表查询
  applyParent: {
    url: '/edu-admin/app/deliver/applyParent',
    method: 'post'
  },
  // 接送申请
  addApply: {
    url: '/edu-admin/app/deliver/applyAdd',
    method: 'post'
  },
  // 学生离校 - 老师确认(含其他代接人信息)
  shuttleForStudent: {
    url: '/edu-admin/app/deliverT/addPerson',
    method: 'post'
  },
  listByStuForMob: {
    url: '/edu-admin/app/person/listByStuForMob',
    method: 'post'
  },
  // 修改密码
  updatePassword: {
    url: '/edu-admin/app/updatePwd',
    method: 'post'
  }
}

export default apiModule
