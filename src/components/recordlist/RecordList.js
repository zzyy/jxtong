/**
 * Created by zzy on 2019/2/15.
 */
export default {
  name: 'RecordList',
  props: {
    type: Number,
    list: {
      type: Array,
      default: []
    },
    loading: {
      type: Boolean,
      default: false
    },
    isFresh: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      recordList: [],
      showLoading: false
    }
  },
  watch: {
    list (val) {
      if (this.isFresh) {
        this.recordList = val
      } else {
        this.recordList = this.recordList.concat(val)
      }
      if (this.recordList.length === 0) {
        this.showLoading = false
      } else {
        this.showLoading = true
      }
    }
  },
  mounted () {
    if (this.recordList.length === 0) {
      this.showLoading = false
    } else {
      this.showLoading = true
    }
  },
  methods: {
    toDetails (data) {
      this.$emit('details', data)
    },
    // 审批操作回调 0-未确认  1-确认  2-拒绝
    approveHandle (id, status) {
      let param = {
        id: id,
        status: status,
        deliverChild: ''
      }

      this.$message.confirm('您确定执行此操作?').then(action => {
        this.$ajax(this.$api.approveConfirm, param).then(
          res => {
            if (res.code === 200) {
              this.$toast({
                message: res.message,
                position: 'bottom',
                duration: 1000
              })
            }
          }
        )
      })
    },
    loadMore () {
      this.$emit('load')
    }
  }
}
