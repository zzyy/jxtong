/**
 * Created by zzy on 2019/2/14.
 */
export default {
  name: 'tabBar',
  props: ['page'],
  data () {
    return {
      selected: this.page
    }
  },
  watch: {
    selected (data) {
      this.$router.push(data)
    }
  }
}
