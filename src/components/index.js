/**
 * Created by zzy on 2019/2/14.
 */
import Tabbar from './tabbar'
import RecordList from './recordlist'
import BackHeader from './backheader'
export {
  Tabbar,
  RecordList,
  BackHeader
}
