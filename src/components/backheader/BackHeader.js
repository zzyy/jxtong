/**
 * Created by zzy on 2019/3/28.
 */
export default {
  name: 'back-header',
  props: {
    title: String
  },
  methods: {
    toBack () {
      this.$router.go(-1)
    }
  }
}
