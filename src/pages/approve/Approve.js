/**
 * Created by zzy on 2019/2/22.
 */
import { RecordList, BackHeader } from 'components'
import { mapGetters, mapMutations } from 'vuex'
export default {
  name: 'approve',
  data () {
    return {
      isParent: false,
      selected: 'all',
      selectedTab: 'tab-all',
      apprMenu: 'ry',
      approveParam: {
        page: 1,
        limit: 10
      },
      parentApproveParam: {
        page: 1,
        limit: 10,
        status: '' // 0 - 未确认  1-已确认  2 - 已拒绝
      },
      parentApprovedParam: {
        page: 1,
        limit: 10,
        status: '' // 0 - 未确认  1-已确认  2 - 已拒绝
      },
      personParam: {
        personName: '',
        beginTime: '',
        cardNum: '',
        childName: '',
        classCode: '',
        endTime: '',
        isExamine: 2,
        limit: 10,
        page: 1
      },
      djShuttleParam: {
        shuttleId: '',
        shuttleName: '',
        shuttlePhone: '',
        type: '0',
        validityDate: '',
        childName: '',
        classCode: '',
        limit: 10,
        page: 1
      },
      dsShuttleParam: {
        shuttleId: '',
        shuttleName: '',
        shuttlePhone: '',
        type: '1',
        validityDate: '',
        childName: '',
        classCode: '',
        limit: 10,
        page: 1
      },
      loading: false,
      loadingPr: false,
      loadingPrd: false,
      loadingRy: false,
      loadingDj: false,
      loadingDs: false,
      showLoadingRy: false,
      showLoadingDj: false,
      showLoadingDs: false,
      approveData: [],
      approvedData: [],
      ryData: [],
      djData: [],
      dsData: []
    }
  },
  computed: {
    ...mapGetters(['personType']),
    pt () {
      return this.personType === '' ? this.$getStore('personType') : this.personType
    }
  },
  components: {
    RecordList,
    BackHeader
  },
  watch: {
    selected (val) {
      this.selectedTab = `tab-${val}`
    }
  },
  mounted () {
    if (this.pt === '6100000000') {
      this.isParent = true
      this.selected = 'all'
      this.selectedTab = 'tab-all'

      this.getParentApprove()
      this.getParentApproved()

      // this.getTeacherApprove()
      // this.getPersonApprove()
      // this.getShuttleApprove(this.djShuttleParam)
      // this.getShuttleApprove(this.dsShuttleParam)
    } else {
      this.isParent = false
      this.selected = 'teachSponsor'
      this.selectedTab = 'tab-teachSponsor'

      this.getTeacherApprove()
      this.getPersonApprove()
      this.getShuttleApprove(this.djShuttleParam)
      this.getShuttleApprove(this.dsShuttleParam)
    }
  },
  methods: {
    ...mapMutations({
      setDetails: 'SET_DETAILS'
    }),
    changeMenu (current) {
      this.apprMenu = current
    },
    // 获取教师申请审批列表
    getTeacherApprove () {
      this.$ajax(this.$api.teacherApply, this.approveParam).then(
        res => {
          if (res.code === 200) {
            res.content.list.forEach(item => {
              item.photo = item.visitorPhoto
            })
            this.approveData = res.content.list
            if (res.content.list.length < this.approveParam.limit) {
              this.loading = true
            }
          }
        }
      )
    },
    // 获取家长全部审批列表
    getParentApprove () {
      this.$ajax(this.$api.getApproveList, this.parentApproveParam).then(
        res => {
          if (res.code === 200) {
            this.approveData = res.content.list
            if (res.content.list.length < this.parentApproveParam.limit) {
              this.loadingPr = true
            }
          }
        }
      )
    },
    // 获取家长已审批列表
    getParentApproved () {
      this.parentApprovedParam.status = '1'
      this.$ajax(this.$api.getApproveList, this.parentApprovedParam).then(
        res => {
          if (res.code === 200) {
            this.approvedData = res.content.list
            if (res.content.list.length < this.parentApprovedParam.limit) {
              this.loadingPrd = true
            }
          }
        }
      )
    },
    // 人员审批
    getPersonApprove () {
      this.$ajax(this.$api.personApprove, this.personParam).then(
        res => {
          if (res.code === 200) {
            this.ryData = res.content.list
            if (this.ryData.length === 0) {
              this.showLoadingRy = false
            } else {
              this.showLoadingRy = true
            }
            if (res.content.list.length < this.personParam.limit) {
              this.loadingRy = true
            }
          }
        }
      )
    },
    // 人员审批操作
    personApproveHandle (type, data) {
      this.$message.confirm('您确定继续该操作吗？').then(action => {
        let param = {
          ids: [],
          isExamine: type
        }
        param.ids.push(data.id)
        this.$ajax(this.$api.controlPersonAppr, param).then(
          res => {
            if (res.code === 200) {
              this.$toast({message: res.message, duration: 1000})
              this.getPersonApprove()
            }
          }
        )
      })
    },
    // 代接送审批 -- type=0-代接，type=1-代送
    getShuttleApprove (param) {
      this.$ajax(this.$api.shuttleApprove, param).then(
        res => {
          if (res.code === 200) {
            if (param.type === '0') {
              this.djData = this.djData.concat(res.content.list)
              if (this.djData.length === 0) {
                this.showLoadingDj = false
              } else {
                this.showLoadingDj = true
              }
              if (res.content.list.length < param.limit) {
                this.loadingDj = true
              }
            } else {
              this.dsData = this.dsData.concat(res.content.list)
              if (this.dsData.length === 0) {
                this.showLoadingDs = false
              } else {
                this.showLoadingDs = true
              }
              if (res.content.list.length < param.limit) {
                this.loadingDs = true
              }
            }
          }
        }
      )
    },
    // 滚动到下一页
    loadNext () {
      this.approveParam.page = this.approveParam.page + 1
      this.getTeacherApprove()
    },
    parentLoadNext () {
      this.parentApproveParam.page = this.parentApproveParam.page + 1
      this.getParentApprove()
    },
    parentLoadNextForApproved () {
      this.parentApproveParam.page = this.parentApproveParam.page + 1
      this.getParentApproved()
    },
    // 加载更多列表
    loadMore (type) {
      switch (type) {
        case 'ry':
          this.personParam.page = this.personParam.page + 1
          this.getPersonApprove()
          break
        case 'dj':
          this.djShuttleParam.page = this.djShuttleParam.page + 1
          this.getShuttleApprove(this.djShuttleParam)
          break
        case 'ds':
          this.dsShuttleParam.page = this.dsShuttleParam.page + 1
          this.getShuttleApprove(this.dsShuttleParam)
          break
      }
    },
    // 我发起的详情
    detailsHandle (data) {
      if (typeof data.id !== 'undefined') {
        this.$router.push({ path: '/approvedetails', query: {id: data.id} })
      } else {
        this.$setStore('approvedetails', data)
        this.$router.push({path: '/approvedetails'})
      }
    },
    sendMeDetailsHandle (title, data) {
      data.title = title
      this.setDetails(data)
      this.$router.push({path: '/sendme'})
    }
  }
}
