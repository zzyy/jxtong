/**
 * Created by zzy on 2019/2/20.
 */
import { BackHeader } from 'components'
export default {
  name: 'fill-in-apply',
  data () {
    return {
      isShow: true,
      typeKey: '',
      typeText: {
        dateLabel: '',
        datePlace: '',
        parentLabel: '',
        parentPlace: '',
        phonePlace: '',
        parentCardLabel: '',
        parentCardPlace: '',
        childLabel: '',
        childPlace: '',
        reasonLabel: '',
        reasonPlace: ''
      },
      parent: {
        personName: '',
        personPhone: ''
      },
      type: '',
      pickerValue: '',
      name: '',
      phone: '',
      cardNum: '',
      popupVisibleType: false,
      popupVisibleChild: false,
      popupVisibleParent: false,
      applyTypes: [
        {key: 'txdj', value: '同校代接', num: '1'},
        {key: 'txds', value: '同校代送', num: '3'},
        {key: 'wxdj', value: '外校代接', num: '2'},
        {key: 'wxds', value: '外校代送', num: '4'}
      ],
      childList: [],
      parentList: [],
      parentNoData: true,
      // 申请参数
      applyParam: {
        type: '',
        validityDate: '',
        shuttleName: '',
        shuttlePhone: '',
        shuttleCardnum: '',
        shuttleChild: '',
        cause: '',
        shuttleId: '',
        childName: '',
        className: ''
      }
    }
  },
  components: {
    BackHeader
  },
  mounted () {
    // 获取申请类型
    let type = this.$route.query.type
    this.applyTypes.forEach(item => {
      if (type === item.key) {
        this.type = item.value
        this.applyParam.type = item.num
      }
    })

    // 是代接还是代送
    if (type === 'txdj' || type === 'wxdj') {
      this.typeText = {
        dateLabel: '代接日期',
        datePlace: '请选择代接日期',
        parentLabel: '代接人',
        parentPlace: '请输入代接人',
        phonePlace: '请输入代接人联系电话',
        parentCardLabel: '代接人身份证',
        parentCardPlace: '请输入代接人身份证号码（选填）',
        childLabel: '被接人',
        childPlace: '请选择被接人',
        reasonLabel: '代接事由',
        reasonPlace: '请填写代接事由'
      }
    } else if (type === 'txds' || type === 'wxds') {
      this.typeText = {
        dateLabel: '代送日期',
        datePlace: '请选择代送日期',
        parentLabel: '代送人',
        parentPlace: '请输入代送人',
        phonePlace: '请输入代送人联系电话',
        parentCardLabel: '代送人身份证',
        parentCardPlace: '请输入代送人身份证号码（选填）',
        childLabel: '被送人',
        childPlace: '请选择被送人',
        reasonLabel: '代送事由',
        reasonPlace: '请填写代送事由'
      }
    }
    // 是外校还是同校
    if (type === 'wxdj' || type === 'wxds') {
      this.isShow = true
    } else if (type === 'txdj' || type === 'txds') {
      this.isShow = false
    }

    // this.getParent()
    this.getChild()
  },
  methods: {
    openPicker () {
      if (this.applyParam.validityDate) {
        this.applyParam.validityDate = this.typeText.datePlace
      } else {
        this.applyParam.validityDate = new Date()
      }

      this.$refs.picker.open()
    },
    confirmHandle (val) {
      this.applyParam.validityDate = val.Format('yyyy-MM-dd')
      this.typeText.datePlace = this.applyParam.validityDate
    },
    // 切换请求类型
    // changeApplyType (data) {
    //   this.type = data.value
    //   this.typeKey = data.key
    //   this.popupVisibleType = false
    //   this.$router.push({path: '/fillinapply', query: {type: data.key}})
    // },
    // 选择代接送人
    selectParentHandle (data) {
      this.applyParam.shuttleName = this.parent.personName
      this.applyParam.shuttleId = data.id
      this.applyParam.className = data.className
      this.popupVisibleParent = false
    },
    // 选中学生
    checkCell (e) {
      let label = e.target.getElementsByClassName('mint-radio-label')[0]
      if (typeof label !== 'undefined') {
        this.applyParam.childName = label.innerText
      }
    },
    // 清空
    clearChecked () {
      this.applyParam.shuttleChild = ''
      this.applyParam.childName = ''
      // this.childList = []

      let listCell = document.getElementsByClassName('list-cell')
      for (let i = 0; i < listCell.length; i++) {
        listCell[i].setAttribute('class', 'list-cell')
      }
    },
    // 确定
    confirmChecked () {
      this.popupVisibleChild = false
    },
    // 取消代接人选择
    cancelParent () {
      this.popupVisibleParent = false
      this.parent = {
        personName: '',
        personPhone: ''
      }
      this.parentList = []
    },
    // 获取同校代接/代送人
    getParent () {
      this.$ajax(this.$api.applyParent, this.parent).then(
        res => {
          if (res.code === 200) {
            if (res.content.length) {
              this.parentNoData = false
              this.parentList = res.content
            } else {
              this.parentNoData = true
            }
          }
        }
      )
    },
    // 获取被接人
    getChild () {
      this.$ajax(this.$api.childList).then(
        res => {
          if (res.code === 200) {
            res.content.forEach(child => {
              let childObj = {
                label: child.personName,
                value: child.id
              }
              this.childList.push(childObj)
            })
          }
        }
      )
    },
    // 提交申请
    submitApply () {
      let name = /^[\u4e00-\u9fa5A-Za-z]+$/u
      let specil = /^[\u4e00-\u9fa5A-Za-z0-9_]+$/u
      let phone = /^[1]([3-9])[0-9]{9}$/
      if (this.applyParam.validityDate === '') { this.$message('提示', '请选择接送日期'); return false }
      if (this.applyParam.shuttleName === '') { this.$message('提示', '请选择代接送人'); return false }
      if (!name.test(this.applyParam.shuttleName)) {
        this.$message('提示', '代接送人不能包含数字及特殊字符')
        return false
      }
      if (this.$route.query.type === 'wxdj' || this.$route.query.type === 'wxds') {
        if (this.applyParam.shuttlePhone === '') { this.$message('提示', '请输入接送人联系电话'); return false }
        if (!phone.test(this.applyParam.shuttlePhone)) {
          this.$message('提示', '联系电话格式错误')
          return false
        }
      }
      if (this.applyParam.shuttleChild === '') { this.$message('提示', '请选择被接送人'); return false }
      if (this.applyParam.cause === '') { this.$message('提示', '请输入代接送事由'); return false }
      if (!specil.test(this.applyParam.cause)) {
        this.$message('提示', '代接送事由不能包含特殊字符')
        return false
      }

      this.$ajax(this.$api.addApply, this.applyParam).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 1000})
            this.$router.go(-1)
          }
        }
      )
    }
  }
}
