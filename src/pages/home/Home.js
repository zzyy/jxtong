/**
 * Created by zzy on 2019/2/14.
 */
import { Tabbar, RecordList } from 'components'
import { mapGetters } from 'vuex'
export default {
  name: 'home',
  data () {
    return {
      currentPage: 'home',
      dataList: [],
      approveData: [],
      classData: [],
      notice: '',
      approveParam: {
        page: 1,
        limit: 10,
        status: '0'
      }
    }
  },
  components: {
    Tabbar,
    RecordList
  },
  mounted () {
    this.getApproveList()
    this.getShuttleList()
    this.getClassBasic()
    this.getNoticeList()

    // this.timer = setInterval(() => {
    //   this.getApproveList()
    // }, 1000 * 5)
  },
  beforeDestroy () {
    clearInterval(this.timer)
  },
  computed: {
    ...mapGetters(['personType']),
    pt () {
      return this.personType === '' ? this.$getStore('personType') : this.personType
    }
  },
  methods: {
    // 获取审批列表
    getApproveList () {
      this.$ajax(this.$api.getApproveList, this.approveParam).then(
        res => {
          if (res.code === 200) {
            this.approveData = []
            if (typeof res.content.list[0] !== 'undefined') {
              this.approveData.push(res.content.list[0])
            }
          }
        }
      )
    },
    // 获取接送人列表
    getShuttleList () {
      this.$ajax(this.$api.getShuttleList).then(
        res => {
          if (res.code === 200) {
            this.dataList = res.content
          }
        }
      )
    },
    // 获取通知列表第一条
    getNoticeList () {
      this.$ajax(this.$api.getNoticeList).then(
        res => {
          if (res.code === 200) {
            if (res.content.length) {
              this.notice = res.content[0].title
            }
          }
        }
      )
    },
    // 老师角色进入，查询班级概况
    getClassBasic () {
      this.$ajax(this.$api.getClassBasic, {}).then(
        res => {
          if (res.code === 200) {
            this.classData = res.content
          }
        }
      )
    },

    // 进入家长审批详情
    toApproveDetails (id) {
      this.$router.push({ path: '/approvedetails', query: {id: id} })
    },
    // 审批操作
    agreeApprove (data, status) {
      let param = {
        id: data.noticeId,
        status: status,
        deliverChild: data.deliverChild
      }
      this.$message.confirm('您确定执行该操作吗？').then(action => {
        this.$ajax(this.$api.approveConfirm, param).then(
          res => {
            if (res.code === 200) {
              this.$toast({
                message: res.message,
                position: 'bottom',
                duration: 1000
              })
              this.$router.go(0)
            }
          }
        )
      })
    },
    // 点击接送记录跳转
    detailsHandle (data) {
      this.$router.push({
        path: '/approvedetails',
        query: {
          id: data.id
        }
      })
    }
  }
}
