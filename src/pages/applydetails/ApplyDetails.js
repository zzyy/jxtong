/**
 * Created by zzy on 2019/2/21.
 */
import QRCode from 'qrcodejs2'
export default {
  name: 'apply-details',
  data () {
    return {
      isShow: false,
      isTx: false,
      typeStr: '接',
      details: {},
      qrcode: '',
      isValid: '二维码已过期'
    }
  },
  mounted () {
    let type = this.$route.query.type
    if (type === 'dj') {
      this.typeStr = '接'
    } else {
      this.typeStr = '送'
    }

    this.getApplyDetails()
    this.setHeight()
  },
  methods: {
    backApply () {
      this.$router.go(-1)
    },
    // 查询详情
    getApplyDetails () {
      let param = {
        url: this.$api.applyDetails.url + this.$route.query.id,
        method: this.$api.applyDetails.method
      }
      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            this.details = res.content
            if (res.content.typeName === '同校代接' || res.content.typeName === '同校代送') {
              this.isTx = true
            }

            let d = new Date()
            let today = d.Format('yyyyMMdd')
            let vDate = res.content.validityDate.replace(/-/g, '')

            if (vDate - today > 0) {
              this.isValid = '二维码暂未生效'
            }

            this.$nextTick(() => {
              this.qrcodeHandle(res.content.qrCode)
            })
          }
        }
      )
    },
    // 生成二维码
    qrcodeHandle (code) {
      this.qrcode = new QRCode('qrcode', {
        width: 80,
        height: 80,
        text: code, // 二维码地址
        colorDark: '#000',
        colorLight: '#fff'
      })
      // setTimeout(() => {
      //   this.isShow = false
      //   let base64 = this.$refs.qrcode.getElementsByTagName('img')[1].src
      //   this.$refs.qrcodeImg.src = base64
      // }, 300)
    },
    saveQrImage (e) {
      this.isShow = true
      let imgWrap = document.getElementsByTagName('body')[0].clientWidth
      this.$refs.imgWrap.style.width = imgWrap + 'px'

      if (e.target.src) {
        this.$refs.fullCode.src = e.target.src
      } else {
        let image = new Image()
        image.src = e.target.toDataURL('image/png')
        this.$refs.fullCode.src = image.src
      }
    },
    setHeight () {
      let app = document.getElementById('app')
      let sendMe = this.$refs.applyDetails
      sendMe.style.height = app.offsetHeight + 'px'
    }
  }
}
