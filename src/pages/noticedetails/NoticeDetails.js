/**
 * Created by zzy on 2019/2/23.
 */
import { BackHeader } from 'components'
export default {
  name: 'notice-details',
  data () {
    return {
      details: {}
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.getNoticeDetail(this.$route.query.id)
    })
  },
  components: {
    BackHeader
  },
  methods: {
    getNoticeDetail (id) {
      let detail = {
        url: this.$api.getNoticeDetail.url + id,
        method: 'get'
      }
      this.$ajax(detail).then(
        res => {
          if (res.code === 200) {
            this.details = res.content
          }
        }
      )
    }
  }
}
