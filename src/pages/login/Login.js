/**
 * Created by zzy on 2019/2/13.
 */
import { mapMutations } from 'vuex'
export default {
  name: 'login',
  data () {
    return {
      form: {
        username: '',
        password: '',
        source: ''
      },
      rolename: '',
      role: '',
      cardNumStu: '',
      cardNumPar: '',
      showEye: false,
      popupVisible: false,
      isLogin: true,
      isParent: false,
      roleList: {'我是家长': '6100000000', '我是老师': '6000000000'}
    }
  },
  watch: {
    rolename (data) {
      if (data === '') {
        this.role = ''
      }
      this.role = this.roleList[data]
      // 家长才能看到学生身份证登录入口
      if (data === '我是家长') {
        this.isParent = true
      } else if (data === '我是老师') {
        this.isParent = false
      }
    }
  },
  computed: {

  },
  methods: {
    ...mapMutations({
      setChildren: 'SET_CHILDREN',
      setParent: 'SET_PARENT',
      setPersonType: 'SET_PERSON_TYPE'
    }),
    // 登录回调
    loginHandle () {
      this.$indicator.open('登录中...')
      this.form.source = this.role
      this.$ajax(this.$api.login, this.form).then(
        res => {
          this.$indicator.close()
          if (res.code === 200) {
            // 保存token
            this.$setStore('token', res.content.token)
            this.$setStore('userId', res.content.entity.userId)
            this.$setStore('userPhone', res.content.entity.personPhone)
            this.$setStore('isEnable', res.content.entity.isEnable)
            this.$setStore('cardNum', res.content.entity.cardNum)

            this.setPersonType(res.content.entity.personType)

            if (res.content.entity.isEnable === '1') {
              // 已激活
              this.$router.push('home')
            } else {
              // 未激活

              // 登录接口返回信息
              let parent = {
                name: res.content.entity.personName,
                cardNum: res.content.entity.cardNum,
                phone: res.content.entity.personPhone,
                photo: res.content.entity.onRegPhotoUrl,
                regPhoto: res.content.entity.regPhotoUrl,
                personId: res.content.entity.userId
              }
              this.setParent(parent)

              this.$router.push('activate')
            }
          }
        }
      )
    },
    // 激活家长回调
    activateHandle () {
      let detailAPI = {
        url: this.$api.childDetail.url + this.cardNumStu + '/' + this.cardNumPar,
        method: this.$api.childDetail.method
      }
      console.log('**************1')
      this.$ajax(detailAPI).then(
        res => {
          if (res.code === 200) {
            if (res.content === null) {
              this.$message('提示', '暂无此学生信息，请联系老师')
            }
            let child = {
              id: res.content[0].id,
              photo: res.content[0].onRegPhotoUrl,
              name: res.content[0].personName,
              grade: res.content[0].className // res.content.gradeName +
            }
            let parent = {
              name: res.content[1].personName,
              cardNum: res.content[1].cardNum,
              phone: res.content[1].personPhone,
              photo: res.content[1].onRegPhotoUrl,
              regPhoto: res.content[1].regPhotoUrl,
              personId: res.content[1].id
            }
            // console.log(child)
            // console.log(parent)
            this.$setStore('isEnable', '1')
            this.setChildren(child)
            this.setParent(parent)
            this.$router.push('activate')
          }
        }
      )
    }
  }
}
