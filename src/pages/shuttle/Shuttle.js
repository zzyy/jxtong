/**
 * Created by zzy on 2019/2/26.
 */
import { BackHeader } from 'components'
export default {
  name: 'shuttle',
  data () {
    return {
      selected: 'lixiao',
      selectedTab: 'tab-lixiao',
      activeGrade1: '',
      activeGrade2: '',
      popupVisible: false,
      studentParams: {
        personName: '',
        classCode: '',
        personState: '',
        limit: 1000
      },
      parentList: [],
      gradeList: [],
      studentList: []
    }
  },
  watch: {
    selected (val) {
      this.selectedTab = `tab-${val}`
      if (val === 'lixiao') {
        this.studentParams.personState = '0'
        this.getStudentList(this.activeGrade1)
      } else if (val === 'daoxiao') {
        this.studentParams.personState = '1'
        this.getStudentList(this.activeGrade2)
      }
    }
  },
  mounted () {
    this.getGradeList()
  },
  components: {
    BackHeader
  },
  methods: {
    selectGradeLX (data) {
      this.activeGrade1 = data.classCode
      this.studentParams.personState = '0'
      this.getStudentList(data.classCode)
    },
    selectGradeDX (data) {
      this.activeGrade2 = data.classCode
      this.studentParams.personState = '1'
      this.getStudentList(data.classCode)
    },
    // 班级列表
    getGradeList () {
      this.$ajax(this.$api.getClassList, {}).then(
        res => {
          if (res.code === 200) {
            this.gradeList = res.content
            if (this.gradeList.length) {
              this.activeGrade1 = this.gradeList[0].classCode
              this.activeGrade2 = this.gradeList[0].classCode

              // 第一次进页面获取第一个班级离校学生
              this.studentParams.personState = '0'
              this.getStudentList(this.gradeList[0].classCode)
            }
          }
        }
      )
    },
    // 学生列表
    getStudentList (code) {
      this.studentParams.classCode = code
      this.$ajax(this.$api.getStudentList, this.studentParams).then(
        res => {
          if (res.code === 200) {
            this.studentList = res.content.list
          }
        }
      )
    },
    // 获取学生所对应的接人列表
    getParentList (type, data) {
      let param
      if (type === 'lx') {
        param = {
          url: this.$api.getParentByStu.url + data.id,
          method: this.$api.getParentByStu.method
        }
      } else {
        param = {
          url: this.$api.getParentByStuD.url + data.id,
          method: this.$api.getParentByStuD.method
        }
      }

      if (data.personState === 2) {
        this.$toast({message: '等待家长确认', duration: 500})
        this.popupVisible = false
        return false
      } else {
        this.popupVisible = true
      }

      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            this.parentList = res.content
          }
        }
      )
    },
    // 老师给家长发送通知
    teacherConfirmHandle (data) {
      this.popupVisible = false
      this.confirmParam = {
        childId: data.childId,
        id: data.id,
        isparent: '',
        applyId: data.applyId,
        type: data.type,
        passId: data.passId
      }

      if (data.jump === '0') {
        if (this.selected === 'lixiao') {
          this.teacherConfirm(this.$api.teacherConfirmR, this.confirmParam)
        } else if (this.selected === 'daoxiao') {
          this.teacherConfirm(this.$api.teacherConfirmD, this.confirmParam)
        }
      } else if (data.jump === '1') {
        this.$router.push({
          path: 'fillinShuttle',
          query: {
            id: data.childId,
            pid: data.applyId,
            type: data.type,
            passid: data.passId,
            st: this.selected
          }
        })
      }
    },
    // 发送通知方法
    teacherConfirm (req, param) {
      this.$ajax(req, param).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 1000})
            // 通知成功，在查询一遍列表
            if (this.selected === 'lixiao') {
              this.studentParams.personState = '0'
              this.getStudentList(this.activeGrade1)
            } else if (this.selected === 'daoxiao') {
              this.studentParams.personState = '1'
              this.getStudentList(this.activeGrade2)
            }
          }
        }
      )
    }
  }
}
