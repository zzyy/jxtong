/**
 * Created by zzy on 2019/2/23.
 */
import { BackHeader } from 'components'
export default {
  name: 'notice',
  data () {
    return {
      isRead: true,
      notice: []
    }
  },
  mounted () {
    this.getNoticeList()
  },
  components: {
    BackHeader
  },
  methods: {
    // 获取通知列表第一条
    getNoticeList () {
      this.$ajax(this.$api.getNoticeList).then(
        res => {
          if (res.code === 200) {
            this.notice = res.content
          }
        }
      )
    },
    toDetails (data) {
      this.$router.push({path: 'noticedetails', query: { id: data.id }})
    }
  }
}
