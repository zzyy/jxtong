/**
 * Created by zzy on 2019/2/19.
 */
import { BackHeader } from 'components'
export default {
  name: 'phone',
  data () {
    return {
      param: {
        entity: {
          id: '',
          personPhone: ''
        }
      }
    }
  },
  mounted () {
    this.param.entity.personPhone = this.$route.query.phone
  },
  components: {
    BackHeader
  },
  methods: {
    submitHandle () {
      let reg = /^[1]([3-9])[0-9]{9}$/
      if (!reg.test(this.param.entity.personPhone)) {
        this.$message('提示', '请检查手机号码格式是否正确')
        return false
      }

      this.param.entity.id = this.$getStore('userId')
      this.$ajax(this.$api.uptPhone, this.param).then(
        res => {
          if (res.code === 200) {
            this.$setStore('userPhone', this.param.entity.personPhone)
            this.$toast({message: res.message, duration: 500})
            this.$router.push('mine')
          }
        }
      )
    }
  }
}
