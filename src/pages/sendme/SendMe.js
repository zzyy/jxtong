/**
 * Created by zzy on 2019/3/11.
 */
import {mapGetters} from 'vuex'
export default {
  name: 'send-me',
  computed: {
    ...mapGetters(['details'])
  },
  mounted () {
    this.setHeight()
  },
  methods: {
    back () {
      this.$router.go(-1)
    },
    setHeight () {
      let app = document.getElementById('app')
      let sendMe = this.$refs.sendMe
      sendMe.style.height = app.offsetHeight + 'px'
    }
  }
}
