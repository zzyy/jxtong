/**
 * Created by zzy on 2019/2/14.
 */
import { Tabbar } from 'components'
import { mapGetters } from 'vuex'
export default {
  name: 'mine',
  data () {
    return {
      currentPage: 'mine',
      phone: '',
      userInfo: {
        avatar: '',
        name: '',
        cardNum: ''
      },
      param: {
        personId: ''
      },
      parentParam: {
        personId: ''
      },
      parentLen: 0,
      childList: []
    }
  },
  computed: {
    ...mapGetters(['personType']),
    pt () {
      return this.personType === '' ? this.$getStore('personType') : this.personType
    }
  },
  mounted () {
    this.phone = this.$getStore('userPhone')
    this.getChildList()
    this.getMineInfo()
  },
  components: {
    Tabbar
  },
  methods: {
    logout () {
      this.$message.confirm('您确定要退出系统吗？').then(action => {
        localStorage.clear()
        this.$router.push('login')
      })
    },
    // 获取我的信息
    getMineInfo () {
      this.$ajax(this.$api.getUserInfo).then(
        res => {
          if (res.code === 200) {
            this.userInfo = {
              avatar: res.content.photo,
              name: res.content.personName,
              cardNum: this.$getStore('cardNum')
            }
          }
        }
      )
    },
    // 获取孩子列表
    getChildList () {
      this.param.personId = this.$getStore('userId')
      this.$ajax(this.$api.childByParent, this.param).then(
        res => {
          if (res.code === 200) {
            this.childList = res.content

            let childId = []
            this.childList.forEach(item => {
              childId.push(item.id)
            })

            if (childId.length) {
              if (this.pt === '6100000000') {
                this.parentParam.personId = childId.join(',')
                this.getParentByStu(this.parentParam)
              }
            }
          }
        }
      )
    },
    // 更新学生信息
    updateChildHandle (data) {
      this.$router.push({path: '/student', query: {id: data.id}})
    },
    // 获取家长个数
    getParentByStu (param) {
      this.$ajax(this.$api.listByStuForMob, param).then(
        res => {
          if (res.code === 200) {
            this.parentLen = res.content.length
          }
        }
      )
    }
  }
}
