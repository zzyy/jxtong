/**
 * Created by zzy on 2019/2/25.
 */
import { BackHeader } from 'components'
import lrz from '../../../static/lrz/dist/lrz.all.bundle'
export default {
  name: 'add-family',
  data () {
    return {
      photo: '',
      prePhoto: '',
      // addDisabled: true,
      popupVisibleChild: false,
      childrenStr: '',
      childList: [],
      parentParam: {
        entity: {
          regPhotoUrl: '',
          personType: '6100000000',
          personName: '',
          personSex: '',
          personRace: '',
          personBirthday: '',
          personAddress: '',
          cardNum: '',
          createId: '',
          personState: '',
          isExamine: '',
          isEnable: '',
          personPhone: '',
          tag: '1', // 1 要审核 2 不要"
          race_cn: ''
        },
        listParent: [
          {
            childId: '', // 学生id
            parentType: ''
          }
        ],
        roleIds: [ ]
      }
    }
  },
  mounted () {
    this.getChildByParent()
    // this.$nextTick(function () {
    //   let u = navigator.userAgent
    //   let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
    //   if (isIOS) {
    //     document.getElementById('photo').removeAttribute('capture', 'camera')
    //   }
    // })
  },
  components: {
    BackHeader
  },
  methods: {
    // 上传照片
    uploadImg (event) {
      let _self = this

      lrz(event.target.files[0], {
        width: 1280
      })
        .then((rst) => {
          _self.photo = rst.base64.split(',')[1]
          _self.prePhoto = rst.base64
        })
        .catch((error) => {
          // 失败时执行
          console.log(error)
        })
    },
    // 上传照片回调
    uploadImgHandle () {
      let param = {
        fileBase64: this.photo,
        cardNum: this.parentParam.entity.cardNum,
        personType: this.parentParam.entity.personType,
        personId: ''
      }

      this.$ajax(this.$api.uploadBase64, param).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 500})
            this.parentParam.entity.regPhotoUrl = res.content
            this.addParentFunc()
          }
        }
      )
    },
    // 添加家人回调
    addHandle () {
      if (this.photo === '') {
        this.$message('提示', '上传照片才能添加哟~')
        return false
      }
      if (this.parentParam.listParent[0].childId === '') {
        this.$message('提示', '请选择学生')
        return false
      }
      if (this.parentParam.entity.personName === '') {
        this.$message('提示', '请输入姓名')
        return false
      }
      if (this.parentParam.entity.personPhone === '') {
        this.$message('提示', '请输入您的联系电话')
        return false
      }

      this.uploadImgHandle()
    },
    addParentFunc () {
      this.parentParam.entity.createId = this.$getStore('userId')
      this.$ajax(this.$api.addParent, this.parentParam).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 500})
            this.$router.go(-1)
          }
        }
      )
    },
    // 选中学生
    checkCell (data, e) {
      let listCell = document.getElementsByClassName('list-cell')

      // 清除所有被选中项
      for (let i = 0; i < listCell.length; i++) {
        listCell[i].setAttribute('class', 'list-cell')
      }

      // 给被选中项添加class
      e.currentTarget.setAttribute('class', 'list-cell is-checked')

      this.childrenStr = data.personName
      this.parentParam.listParent[0].childId = data.id
    },
    // 清空
    clearChecked () {
      this.childrenStr = ''

      let listCell = document.getElementsByClassName('list-cell')
      for (let i = 0; i < listCell.length; i++) {
        listCell[i].setAttribute('class', 'list-cell')
      }
    },
    // 确定
    confirmChecked () {
      this.popupVisibleChild = false
    },
    // 家长查看子女列表
    getChildByParent () {
      let param = {
        personId: this.$getStore('userId')
      }
      this.$ajax(this.$api.childByParent, param).then(
        res => {
          if (res.code === 200) {
            this.childList = res.content
          }
        }
      )
    }
  }
}
