/**
 * Created by zzy on 2019/2/14.
 */
import { Tabbar } from 'components'
export default {
  name: 'school',
  data () {
    return {
      currentPage: 'school'
    }
  },
  components: {
    Tabbar
  }
}
