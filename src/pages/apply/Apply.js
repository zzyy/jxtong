/**
 * Created by zzy on 2019/2/19.
 */
import { RecordList, BackHeader } from 'components'
export default {
  name: 'apply',
  data () {
    return {
      selected: 'dj',
      selectedTab: 'tab-dj',
      djParam: {
        validityDate: '',
        childName: '',
        classCode: '',
        shuttleName: '',
        shuttlePhone: '',
        shuttleId: '',
        type: ''
      },
      dsParam: {
        validityDate: '',
        childName: '',
        classCode: '',
        shuttleName: '',
        shuttlePhone: '',
        shuttleId: '',
        type: ''
      },
      djDataList: [],
      dsDataList: []
    }
  },
  components: {
    RecordList,
    BackHeader
  },
  watch: {
    selected (val) {
      this.selectedTab = `tab-${val}`
    }
  },
  mounted () {
    this.getDjList()
    this.getDsList()
  },
  methods: {
    getDjList () {
      this.$ajax(this.$api.djApply, this.djParam).then(
        res => {
          if (res.code === 200) {
            res.content.forEach(item => {
              item.visitorName = item.shuttleName
              item.photo = item.childPhoto
              item.confirmTime = item.validityDate
              if (item.status === '0') {
                item.status = '未确认'
              } else if (item.status === '1') {
                item.status = '已确认'
              } else if (item.status === '2') {
                item.status = '已拒绝'
              }
            })
            this.djDataList = res.content
          }
        }
      )
    },
    getDsList () {
      this.$ajax(this.$api.dsApply, this.dsParam).then(
        res => {
          if (res.code === 200) {
            res.content.forEach(item => {
              item.visitorName = item.shuttleName
              item.photo = item.childPhoto
              item.confirmTime = item.validityDate
              if (item.status === '0') {
                item.status = '未确认'
              } else if (item.status === '1') {
                item.status = '已确认'
              } else if (item.status === '2') {
                item.status = '已拒绝'
              }
            })
            this.dsDataList = res.content
          }
        }
      )
    },
    // 获取代接详情
    getDjDetail (data) {
      this.$router.push({path: '/applydetails', query: {id: data.id, type: 'dj'}})
    },
    // 获取代送详情
    getDsDetail (data) {
      this.$router.push({path: '/applydetails', query: {id: data.id, type: 'ds'}})
    }
  }
}
