/**
 * Created by zzy on 2019/2/23.
 */
import { mapGetters } from 'vuex'
import { BackHeader } from 'components'
export default {
  name: 'approve-details',
  data () {
    return {
      isShow: false,
      param: {
        id: '',
        status: '',
        deliverChild: ''
      },
      details: {}
    }
  },
  computed: {
    ...mapGetters(['personType']),
    pt () {
      return this.personType === '' ? this.$getStore('personType') : this.personType
    }
  },
  components: {
    BackHeader
  },
  mounted () {
    let localData = JSON.parse(localStorage.getItem('approvedetails'))
    if (localData === null) {
      this.getApproveDetails()
    } else {
      this.details = localData
    }
  },
  methods: {
    // 查询详情
    getApproveDetails () {
      let param = {
        url: this.$api.getApproveDetail.url + this.$route.query.id,
        method: this.$api.getApproveDetail.method
      }
      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            res.content.status = res.content.status === '0' ? '未确认' : res.content.status === '1' ? '已确认' : '已拒绝'

            this.deliverChild = res.content.deliverChild
            this.details = res.content
          }
        }
      )
    },
    agreeApprove (status) {
      this.param.id = this.details.noticeId
      this.param.status = status
      this.$message.confirm('您确定执行该操作吗？').then(action => {
        this.$ajax(this.$api.approveConfirm, this.param).then(
          res => {
            if (res.code === 200) {
              this.$toast({
                message: res.message,
                position: 'bottom',
                duration: 1000
              })
              this.$router.go(-1)
            }
          }
        )
      })
    },
    saveQrImage (e) {
      this.isShow = true
      let imgWrap = document.getElementsByTagName('body')[0].clientWidth
      this.$refs.imgWrap.style.width = imgWrap + 'px'

      if (e.target.src) {
        this.$refs.fullCode.src = e.target.src
      } else {
        let image = new Image()
        image.src = e.target.toDataURL('image/png')
        this.$refs.fullCode.src = image.src
      }
    }
  }
}
