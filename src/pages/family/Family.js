/**
 * Created by zzy on 2019/2/25.
 */
import { BackHeader } from 'components'
export default {
  name: 'family',
  data () {
    return {
      parentParam: {
        personId: ''
      },
      familyList: []
    }
  },
  mounted () {
    this.getParentByStu()
  },
  components: {
    BackHeader
  },
  methods: {
    addFamily () {
      this.$router.push('addfamily')
    },
    familyDetails (id) {
      this.$router.push({path: 'familydetails', query: {id: id}})
    },
    // 获取家长
    getParentByStu () {
      this.parentParam.personId = this.$route.query.id
      this.$ajax(this.$api.listByStuForMob, this.parentParam).then(
        res => {
          if (res.code === 200) {
            res.content.forEach(entity => {
              let tempArr = []
              if (entity.entity === null) {
                entity.entity = {}
              }
              entity.listParent.forEach(parent => {
                tempArr.push(parent.parentType)
              })
              entity.parentType = tempArr.join(',')
            })
            this.familyList = res.content
          }
        }
      )
    }
  }
}
