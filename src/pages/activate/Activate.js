/**
 * Created by zzy on 2019/2/19.
 */
import { mapGetters } from 'vuex'
import { BackHeader } from 'components'
import lrz from '../../../static/lrz/dist/lrz.all.bundle'
export default {
  name: 'activate',
  data () {
    return {
      form: {
        entity: {
          personName: '',
          cardNum: '',
          regPhotoUrl: '',
          personPhone: '',
          personType: '6100000000',

          personSex: '',
          personRace: '',
          race_cn: '',
          personBirthday: '',
          personAddress: '',
          createId: '',
          personState: '',
          isExamine: '1',
          isEnable: '1',
          stuId: ''
        }
      },
      isUpLoad: false,
      isChild: true,
      photo: '',
      prePhoto: '',
      parentId: ''
    }
  },
  computed: {
    ...mapGetters(['children', 'parent'])
  },
  components: {
    BackHeader
  },
  mounted () {
    // 若为1则显示孩子信息（1已激活），若为其他则不显示
    this.$getStore('isEnable') === '1' ? this.isChild = true : this.isChild = false

    this.form.entity.personName = this.parent.name
    this.form.entity.cardNum = this.parent.cardNum
    this.form.entity.personPhone = this.parent.phone
    this.form.entity.regPhotoUrl = this.parent.regPhoto
    this.parentId = this.parent.personId
    this.prePhoto = this.imgJoint(this.parent.photo)
  },
  methods: {
    // 上传照片
    uploadImg (event) {
      this.isUpLoad = true
      let _self = this

      lrz(event.target.files[0], {
        width: 1280
      })
        .then((rst) => {
          this.photo = rst.base64.split(',')[1]
          _self.prePhoto = rst.base64
        })
        .catch((error) => {
          // 失败时执行
          console.log(error)
        })
    },
    // 上传照片回调
    uploadImgHandle () {
      let param = {
        fileBase64: this.photo,
        cardNum: this.form.entity.cardNum,
        personType: this.form.entity.personType,
        personId: this.parentId
      }

      this.$ajax(this.$api.uploadBase64, param).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 500})
            this.form.entity.regPhotoUrl = res.content
            this.activate()
          }
        }
      )
    },
    // 激活方法
    activate () {
      if (this.isChild) {
        // 学生身份证激活
        this.form.entity.stuId = this.children.id
        this.$ajax(this.$api.uptParForMobByStu, this.form).then(
          res => {
            if (res.code === 200) {
              this.$toast({message: res.message, duration: 1500})
              this.$router.push('login')
            }
          }
        )
      } else {
        // 家长身份证激活
        delete this.form.stuId
        this.form.entity.id = this.parent.personId
        this.$ajax(this.$api.uptParForMobByOwn, this.form).then(
          res => {
            if (res.code === 200) {
              this.$toast({message: res.message, duration: 1500})
              this.$router.push('login')
            }
          }
        )
      }
    },
    // 点击激活按钮回调
    activateHandle () {
      if (this.isUpLoad) {
        if (this.photo === '') {
          this.$message('提示', '上传照片才能激活哟~')
          return false
        }
        this.uploadImgHandle()
      } else {
        this.activate()
      }
    }
  }
}
