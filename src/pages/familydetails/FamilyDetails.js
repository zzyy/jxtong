/**
 * Created by zzy on 2019/2/25.
 */
export default {
  name: 'family-details',
  data () {
    return {
      isShow: false,
      parent: ''
    }
  },
  mounted () {
    this.setHeight()
    this.getParentetails()
  },
  methods: {
    backPage () {
      this.$router.go(-1)
    },
    // 查询家长详情
    getParentetails () {
      let param = {
        url: this.$api.parentDetails.url + this.$route.query.id,
        method: this.$api.parentDetails.method
      }
      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            this.parent = res.content.entity
            this.parent.relationName = res.content.listParent[0].parentType
          }
        }
      )
    },
    setHeight () {
      let app = document.getElementById('app')
      let sendMe = this.$refs.familyDetail
      sendMe.style.height = app.offsetHeight + 'px'
    },
    saveQrImage (e) {
      this.isShow = true
      let imgWrap = document.getElementsByTagName('body')[0].clientWidth
      this.$refs.imgWrap.style.width = imgWrap + 'px'

      if (e.target.src) {
        this.$refs.fullCode.src = e.target.src
      } else {
        let image = new Image()
        image.src = e.target.toDataURL('image/png')
        this.$refs.fullCode.src = image.src
      }
    }
  }
}
