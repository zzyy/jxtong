/**
 * Created by zzy on 2019/2/26.
 */
import { BackHeader } from 'components'
export default {
  name: 'fill-in-shuttle',
  data () {
    return {
      children: {},
      type: {
        name: {label: '代接人', text: '请输入代接人姓名'},
        phone: {label: '联系电话', text: '请输入代接人联系电话'},
        cardNum: {label: '代接人证件号', text: '请输入代接人证件号（选填）'},
        cause: {label: '代接原因', text: '请输入代接原因（选填）'}
      },
      photo: '',
      prePhoto: '',
      shuttleParam: {
        childId: '',
        visitorName: '',
        visitorCardnum: '',
        visitorPhone: '',
        visitorPhoto: '', // base64
        visitorDescribe: '',
        applyId: '',
        type: '6',
        passId: ''
      }
    }
  },
  components: {
    BackHeader
  },
  mounted () {
    if (this.$route.query.st === 'lixiao') {
      this.type = {
        name: {label: '代接人', text: '请输入代接人姓名'},
        phone: {label: '联系电话', text: '请输入代接人联系电话'},
        cardNum: {label: '代接人证件号', text: '请输入代接人证件号（选填）'},
        cause: {label: '代接原因', text: '请输入代接代接原因（选填）'}
      }
    } else if (this.$route.query.st === 'daoxiao') {
      this.type = {
        name: {label: '代送人', text: '请输入代送人姓名'},
        phone: {label: '联系电话', text: '请输入代送人联系电话'},
        cardNum: {label: '代送人证件号', text: '请输入代送人证件号（选填）'},
        cause: {label: '代送原因', text: '请输入代送原因（选填）'}
      }
    }

    this.getChildDetail()
  },
  methods: {
    // 查询学生详情
    getChildDetail () {
      let param = {
        url: this.$api.childDetailForPC.url + this.$route.query.id,
        method: this.$api.childDetailForPC.method
      }
      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            this.children = res.content.entity
          }
        }
      )
    },
    // 上传照片
    uploadImg (event) {
      let _self = this
      this.photo = event.target.files[0]

      if (!event || !window.FileReader) return false // 判断支持不支持FileReader
      let reader = new FileReader()
      reader.readAsDataURL(this.photo)
      reader.onloadend = function () {
        _self.shuttleParam.visitorPhoto = this.result.split('data:image/jpeg;base64,')[1]
        _self.prePhoto = this.result
      }
    },
    // 提交请求
    submitShuttleApply () {
      this.shuttleParam.childId = this.$route.query.id
      this.shuttleParam.applyId = this.$route.query.pid || ''
      this.shuttleParam.type = this.$route.query.type || ''
      this.shuttleParam.passId = this.$route.query.passid || ''

      if (this.shuttleParam.visitorName === '') { this.$message('提示', '请输入接送人姓名'); return false }
      if (this.shuttleParam.visitorPhone === '') { this.$message('提示', '请输入接送人联系电话'); return false }
      if (this.shuttleParam.visitorPhoto === '') { this.$message('提示', '请上传接送人照片'); return false }

      this.$ajax(this.$api.shuttleForStudent, this.shuttleParam).then(
        res => {
          if (res.code === 200) {
            this.$toast({
              message: res.message,
              duration: 1000
            })
            this.$router.go(-1)
          }
        }
      )
    }
  }
}
