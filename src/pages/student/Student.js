/**
 * Created by zzy on 2019/3/12.
 */
import { BackHeader } from 'components'
export default {
  name: 'student',
  data () {
    return {
      isShow: false,
      child: {},
      relation: ''
    }
  },
  mounted () {
    this.getChild()
  },
  components: {
    BackHeader
  },
  methods: {
    getChild () {
      let param = {
        url: this.$api.getChildDetail.url + this.$route.query.id + '/' + this.$getStore('userId'),
        method: this.$api.getChildDetail.method
      }
      this.$ajax(param).then(
        res => {
          if (res.code === 200) {
            this.child = res.content
            this.relation = res.content.relationName
          }
        }
      )
    },
    // 修改与子女的关系
    submitHandle () {
      let submitParam = {
        childId: this.$route.query.id,
        parentId: this.$getStore('userId'),
        parentType: this.relation
      }
      this.$ajax(this.$api.relationBind, submitParam).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: res.message, duration: 500})
            this.$router.go(-1)
          }
        }
      )
    },
    saveQrImage (e) {
      this.isShow = true
      let imgWrap = document.getElementsByTagName('body')[0].clientWidth
      this.$refs.imgWrap.style.width = imgWrap + 'px'

      if (e.target.src) {
        this.$refs.fullCode.src = e.target.src
      } else {
        let image = new Image()
        image.src = e.target.toDataURL('image/png')
        this.$refs.fullCode.src = image.src
      }
    }
  }
}
