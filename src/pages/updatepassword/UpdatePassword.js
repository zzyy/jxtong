import { BackHeader } from 'components'
import { getStore } from '@/utils/storage'
export default {
  name: 'UpdatePassword',
  data () {
    return {
      params: {
        userid: '',
        passwordOld: '',
        password: ''
      }
    }
  },
  components: {
    BackHeader
  },
  mounted () {
    this.params.userid = getStore('userId')
  },
  methods: {
    submitHandle () {
      let password = /[a-zA-Z0-9_]{6,12}/
      if (!password.test(this.params.passwordOld)) {
        this.$message('提示', '请检查旧密码格式是否正确')
        return false
      }
      if (!password.test(this.params.password)) {
        this.$message('提示', '请检查新密码格式是否正确')
        return false
      }
      this.$ajax(this.$api.updatePassword, this.params).then(
        res => {
          if (res.code === 200) {
            this.$toast({message: '密码修改成功，请重新登录'})
            this.$router.push('login')
          }
        }
      )
    }
  }
}
