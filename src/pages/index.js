/**
 * Created by zzy on 2019/2/13.
 */
import Login from './login'
import Home from './home'
import School from './school'
import Mine from './mine'
import Phone from './phone'
import Activate from './activate'
import Apply from './apply'
import FillInApply from './fillinapply'
import ApplyDetails from './applydetails'
import Approve from './approve'
import ApproveDetails from './approvedetails'
import Notice from './notice'
import NoticeDetails from './noticedetails'
import Family from './family'
import AddFamily from './addFamily'
import FamilyDetails from './familydetails'
import Shuttle from './shuttle'
import FillInShuttle from './fillinShuttle'
import SendMe from './sendme'
import Student from './student'
import UpdatePassword from './updatepassword'
export {
  Login,
  Home,
  School,
  Mine,
  Phone,
  Activate,
  Apply,
  FillInApply,
  ApplyDetails,
  Approve,
  ApproveDetails,
  Notice,
  NoticeDetails,
  Family,
  AddFamily,
  FamilyDetails,
  Shuttle,
  FillInShuttle,
  SendMe,
  Student,
  UpdatePassword
}
