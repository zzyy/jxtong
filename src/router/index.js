import Vue from 'vue'
import Router from 'vue-router'
import {
  Login,
  Home,
  School,
  Mine,
  Phone,
  Activate,
  Apply,
  FillInApply,
  ApplyDetails,
  Approve,
  ApproveDetails,
  Notice,
  NoticeDetails,
  Family,
  AddFamily,
  FamilyDetails,
  Shuttle,
  FillInShuttle,
  SendMe,
  Student,
  UpdatePassword
} from 'pages'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/approvedetails',
          name: 'approvedetails',
          component: ApproveDetails
        }
      ]
    },
    {
      path: '/school',
      name: 'school',
      component: School
    },
    {
      path: '/mine',
      name: 'mine',
      component: Mine
    },
    {
      path: '/phone',
      name: 'phone',
      component: Phone
    },
    {
      path: '/updatepassword',
      name: 'updatepassword',
      component: UpdatePassword
    },
    {
      path: '/activate',
      name: 'activate',
      component: Activate
    },
    {
      path: '/apply',
      name: 'apply',
      component: Apply,
      children: [
        {
          path: '/applydetails',
          name: 'applydetails',
          component: ApplyDetails
        }
      ]
    },
    {
      path: '/fillinapply',
      name: 'fillinapply',
      component: FillInApply
    },
    {
      path: '/approve',
      name: 'approve',
      component: Approve,
      children: [
        {
          path: '/sendme',
          name: 'sendme',
          component: SendMe
        }
      ]
    },
    {
      path: '/approvedetails',
      name: 'approvedetails',
      component: ApproveDetails
    },
    {
      path: '/notice',
      name: 'notice',
      component: Notice
    },
    {
      path: '/noticedetails',
      name: 'noticedetails',
      component: NoticeDetails
    },
    {
      path: '/family',
      name: 'family',
      component: Family,
      children: [
        {
          path: '/familydetails',
          name: 'familydetails',
          component: FamilyDetails
        }
      ]
    },
    {
      path: '/addfamily',
      name: 'addfamily',
      component: AddFamily
    },
    {
      path: '/shuttle',
      name: 'shuttle',
      component: Shuttle
    },
    {
      path: '/fillinshuttle',
      name: 'fillinshuttle',
      component: FillInShuttle
    },
    {
      path: '/student',
      name: 'student',
      component: Student
    }
  ]
})
