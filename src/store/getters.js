/**
 * Created by zzy on 2019/2/28.
 */
export const children = state => state.children
export const parent = state => state.parent
export const personType = state => state.personType
export const details = state => state.details
