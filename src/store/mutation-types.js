/**
 * Created by zzy on 2019/2/28.
 */
export const SET_CHILDREN = 'SET_CHILDREN'
export const SET_PARENT = 'SET_PARENT'
export const SET_PERSON_TYPE = 'SET_PERSON_TYPE'
export const SET_DETAILS = 'SET_DETAILS'
