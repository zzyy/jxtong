/**
 * Created by zzy on 2019/2/28.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import state from './state'
import mutations from './mutations'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

// 单例模式
export default new Vuex.Store({
  actions,
  getters,
  state,
  mutations,
  strict: debug, // 生产环境开启严格模式
  plugins: debug ? [createLogger()] : []
})
