/**
 * Created by zzy on 2019/2/28.
 */
import * as types from './mutation-types'
import { setStore } from '../utils/storage'

const mutations = {
  // 这里第一个参数state是获取当前状态树上的state，第二个是我们传的参数
  [types.SET_CHILDREN] (state, children) {
    state.children = children
  },
  [types.SET_PARENT] (state, parent) {
    state.parent = parent
  },
  [types.SET_PERSON_TYPE] (state, personType) {
    setStore('personType', personType)
    state.personType = personType
  },
  [types.SET_DETAILS] (state, details) {
    state.details = details
  }
}

export default mutations
