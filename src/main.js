// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import 'mint-ui/lib/style.css'
import '../static/js/flexible.js'
import {
  Button,
  Toast,
  Field,
  Popup,
  Tabbar,
  TabItem,
  TabContainer,
  TabContainerItem,
  Cell,
  MessageBox,
  Navbar,
  DatetimePicker,
  Checklist,
  InfiniteScroll,
  Spinner,
  Indicator,
  Header,
  Radio
} from 'mint-ui'
import api from './api'
import { ajax, setStore, getStore, removeStore } from './utils'
import store from './store'

// import Vconsole from 'vconsole'
// const vConsole = new Vconsole()
// export default vConsole

Vue.config.productionTip = false

Vue.component(Button.name, Button)
Vue.component(Field.name, Field)
Vue.component(Popup.name, Popup)
Vue.component(Tabbar.name, Tabbar)
Vue.component(TabItem.name, TabItem)
Vue.component(TabContainer.name, TabContainer)
Vue.component(TabContainerItem.name, TabContainerItem)
Vue.component(Cell.name, Cell)
Vue.component(Navbar.name, Navbar)
Vue.component(DatetimePicker.name, DatetimePicker)
Vue.component(Checklist.name, Checklist)
Vue.use(InfiniteScroll)
Vue.component(Spinner.name, Spinner)
Vue.component(Header.name, Header)
Vue.component(Radio.name, Radio)
Vue.prototype.$toast = Toast
Vue.prototype.$message = MessageBox
Vue.prototype.$indicator = Indicator

Vue.prototype.$setStore = setStore
Vue.prototype.$getStore = getStore
Vue.prototype.$removeStore = removeStore
Vue.prototype.$api = api
Vue.prototype.$ajax = ajax

// this.$upload.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
// this.$upload.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-Requested-With,Content-Type'
// this.$upload.defaults.headers.common['Access-Control-Allow-Methods'] = 'PUT,POST,GET,DELETE,OPTIONS'

// 上传文件
const upload = axios.create({
  baseURL: process.env.API_URL
  // withCredentials: true
  // headers: {
  //   'X-Requested-with': 'XMLHttpRequest',
  //   'Content-Type': 'application/x-www-form-urlencoded '
  // }
})
Vue.prototype.$upload = upload

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { // author: meizz
  let o = {
    'M+': this.getMonth() + 1, // 月份
    'd+': this.getDate(), // 日
    'h+': this.getHours(), // 小时
    'm+': this.getMinutes(), // 分
    's+': this.getSeconds(), // 秒
    'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
    'S': this.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

// 图片拼接real rubbish
Vue.prototype.imgJoint = function (url) {
  const root = process.env.API_URL + '/edu-admin'
  return root + url
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
